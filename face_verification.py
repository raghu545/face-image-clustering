#!/usr/bin/env python
# coding: utf-8
# author: huangyangyu

layer_num = 27
# layer_num = 64

import os
import sys
import time
import gflags
import _pickle as cPickle
import numpy as np
import cv2
import schedule
import natsort

from collections import OrderedDict
import glob

# root_dir = os.path.dirname(os.path.abspath("__file__")) + "/../../"
root_dir = './'
# sys.path.append(root_dir + "code/")


if not gflags.FLAGS.has_key("model_dir"):
    gflags.DEFINE_string("model_dir", './' + "model_face_verifiction/ResNet-%d" % layer_num, "set model dir")
if not gflags.FLAGS.has_key("feature_layer_names"):
    gflags.DEFINE_string("feature_layer_names", "['fc5']", "set feature layer names")
if not gflags.FLAGS.has_key("device_id"):
    gflags.DEFINE_integer("device_id", -1, "set device id")
if not gflags.FLAGS.has_key("ratio"):
    gflags.DEFINE_float("ratio", -1.0, "set image ratio")
if not gflags.FLAGS.has_key("scale"):
    gflags.DEFINE_float("scale", 1.1, "set image scale")
if not gflags.FLAGS.has_key("resize_height"):
    gflags.DEFINE_integer("resize_height", 144, "set image height")
if not gflags.FLAGS.has_key("resize_width"):
    gflags.DEFINE_integer("resize_width", 144, "set image width")
if not gflags.FLAGS.has_key("raw_scale"):
    gflags.DEFINE_float("raw_scale", 255.0, "set raw scale")
if not gflags.FLAGS.has_key("input_scale"):
    gflags.DEFINE_float("input_scale", 0.0078125, "set raw scale")
if not gflags.FLAGS.has_key("gray"):
    gflags.DEFINE_boolean("gray", False, "set gray")
if not gflags.FLAGS.has_key("oversample"):
    gflags.DEFINE_boolean("oversample", False, "set oversample")

from featurer import Featurer


def cos_sim(v1, v2):
    v1 /= np.linalg.norm(v1)
    v2 /= np.linalg.norm(v2)
    dist = np.linalg.norm(np.array(v1) - np.array(v2))
    cos = 1 - dist * dist / 2
    return cos


def get_feature(featurer, image_dir, feature_dim=512):
    final_feature = np.zeros(feature_dim)
    for name in glob.glob(image_dir + '/*.png'):
        # image_file = image_dir + name
        feature = featurer.test(image_file=name)
        if feature is not None:
            final_feature += feature / np.linalg.norm(feature, ord=2)
        final_feature /= np.linalg.norm(final_feature, ord=2)
    return final_feature


def job():
    log = 'log/PersonID*'
    # pairs
    # image_files = np.sort(glob.glob(log))

    file = open("log/logs.txt", 'a+')

    file.seek(0)
    
    #import pdb
    #pdb.set_trace()
    
    if not file.readlines():
        offset=0
    else:
        file.seek(0)
        last_folder = file.readlines()[-1]
        offset = int(''.join(filter(str.isdigit, last_folder)))

    

    image_files = natsort.natsorted(glob.glob(log))
    
    

    #image_files = [*filter(os.path.isdir, image_files)]

    

    image_files = image_files[offset:]
    #import pdb
    #pdb.set_trace()
    print(image_files)

    feature_file = 'log' + "/feature_%d.pkl" % layer_num

    # features
    # feature_file = image_dir + "feature_%d.pkl" % layer_num
    recalculate = True
    if not os.path.exists(feature_file) or recalculate:
        gflags.FLAGS(sys.argv)
        model_dir = gflags.FLAGS.model_dir + "/"
        featurer = Featurer(deploy_prototxt=model_dir + "deploy.prototxt",
                            model_file=model_dir + "train.caffemodel",
                            mean_file=model_dir + "mean.binaryproto",
                            ratio_file=model_dir + "ratio.txt",
                            label_file=model_dir + "label.txt",
                            device_id=gflags.FLAGS.device_id,
                            ratio=gflags.FLAGS.ratio,
                            scale=gflags.FLAGS.scale,
                            resize_height=gflags.FLAGS.resize_height,
                            resize_width=gflags.FLAGS.resize_width,
                            raw_scale=gflags.FLAGS.raw_scale,
                            input_scale=gflags.FLAGS.input_scale,
                            gray=gflags.FLAGS.gray,
                            oversample=gflags.FLAGS.oversample,
                            feature_layer_names=eval(gflags.FLAGS.feature_layer_names))
                            
        if not os.path.exists(feature_file):
            features = OrderedDict()
        else:
            features = cPickle.load(open(feature_file, "rb"))
            

        if image_files:
            for k, image_dir in enumerate(image_files):
                # print(image_files)
                features[image_dir.replace(root_dir, "")] = get_feature(featurer, image_dir)

            cPickle.dump(features, open(feature_file, "wb"))
            file.write(image_dir + '\n')
            file.close()

    else:

        features = cPickle.load(open(feature_file, "rb"))
        features = list(features.values())
        feature1 = features[0]
        for feature in features:
            sim_score = cos_sim(feature, feature1)
            print(sim_score)


if __name__ == "__main__":
    schedule.every(0.25).minutes.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)
