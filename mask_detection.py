
# import the necessary packages
#from pyimagesearch.centroidtracker import CentroidTracker
#from pyimagesearch.trackableobject import TrackableObject
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import os
#from datetime import datetime
from utils.anchor_generator import generate_anchors
from utils.anchor_decode import decode_bbox
from utils.nms import single_class_non_max_suppression

from load_model.tensorflow_loader import load_tf_model, tf_inference

#from elasticsearch import Elasticsearch

#import csv

#csv_path ='/images/events_masks.csv'
#with open(csv_path,'w') as f:
#    writer = csv.writer(f)
#    headers = ['unique_id', 'timestamp','mask']
#    writer.writerow(headers)


#es = Elasticsearch()

sess, graph = load_tf_model('models/face_mask_detection.pb')


model_dir = 'model_face_verifiction/ResNet-27/'


# anchor configuration
feature_map_sizes = [[33, 33], [17, 17], [9, 9], [5, 5], [3, 3]]
anchor_sizes = [[0.04, 0.056], [0.08, 0.11], [0.16, 0.22], [0.32, 0.45], [0.64, 0.72]]
anchor_ratios = [[1, 0.62, 0.42]] * 5

# generate anchors
anchors = generate_anchors(feature_map_sizes, anchor_sizes, anchor_ratios)

# for inference , the batch size is 1, the model output shape is [1, N, 4],
# so we expand dim for anchors to [1, anchor_num, 4]
anchors_exp = np.expand_dims(anchors, axis=0)

import _pickle as cPickle

layer_num = 27
# feature_file = "database/" + "feature_%d.pkl" % layer_num
# features = cPickle.load(open(feature_file, "rb"))

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-i", "--input", type=str,default="/dev/video0",
                help="path to optional input video file")
ap.add_argument("-o", "--output", type=str,
                help="path to optional output video file")
ap.add_argument("-c", "--confidence", type=float, default=0.4,
                help="minimum probability to filter weak detections")
ap.add_argument("-s", "--skip-frames", type=int, default=10,
                help="# of skip frames between detections")
args = vars(ap.parse_args())

vc = cv2.VideoCapture(args['input'])
print(args['input'])

# initialize the video writer (we'll instantiate later if need be)
writer = None

# initialize the frame dimensions (we'll set them as soon as we read
# the first frame from the video)
W = None
H = None

# instantiate our centroid tracker, then initialize a list to store
# each of our dlib correlation trackers, followed by a dictionary to
# map each unique object ID to a TrackableObject
#ct = CentroidTracker(maxDisappeared=40, maxDistance=50)
trackers = []
trackableObjects = {}

# initialize the total number of frames processed thus far, along
# with the total number of objects that have moved either up or down
totalFrames = 0
totalDown = 0
totalUp = 0

# start the frames per second throughput estimator
fps = FPS().start()

target_shape = (260, 260)

conf_thresh = 0.5
iou_thresh = 0.5

# loop over frames from the video stream

status = True
import glob


def cos_sim(v1, v2):
    v1 /= np.linalg.norm(v1)
    v2 /= np.linalg.norm(v2)
    dist = np.linalg.norm(np.array(v1) - np.array(v2))
    cos = 1 - dist * dist / 2
    return cos


unique_id = 0

folders = glob.glob("ObjID*/")

offset = len(folders)

trackers_count = 0

offset_ = 0


import imagezmq


imageHub = imagezmq.ImageHub()
while True:
    start_stamp = time.time()
    status, frame = vc.read()
    #(deviceName, frame) = imageHub.recv_image()
    #print(frame.shape)
    #imageHub.send_reply(b'OK')
    read_frame_stamp = time.time()

    # pdb.set_trace()
    # frame = frame[1] if args.get("input", False) else frame

    # print(status)
    #if status is False:
    #    # print("BREAKING")
    #    break

    # resize the frame to have a maximum width of 500 pixels (the
    # less data we have, the faster we can process it), then convert
    # the frame from BGR to RGB for dlib

    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # if we are supposed to be writing a video to disk, initialize
    # the writer
    # if args["output"] is not None and writer is None:
    # 	fourcc = cv2.VideoWriter_fourcc(*"MJPG")
    # 	writer = cv2.VideoWriter(args["output"], fourcc, 30,
    # 		(W, H), True)

    # initialize the current status along with our list of bounding
    # box rectangles returned by either (1) our object detector or
    # (2) the correlation trackers
    status = "Waiting"
    rects = []
    frames = []
    # check to see if we should run a more computationally expensive
    # object detection method to aid our tracker

    if totalFrames % args["skip_frames"] == 0:

        

        # set the status and initialize our new set of object trackers
        status = "Detecting"
        trackers = []

        height, width, _ = frame.shape

        image_resized = cv2.resize(rgb, target_shape)
        image_np = image_resized / 255.0  #
        image_exp = np.expand_dims(image_np, axis=0)

        image_transposed = image_exp.transpose((0, 3, 1, 2))
        # print(image_transposed.shape)
        y_bboxes_output, y_cls_output = tf_inference(sess, graph, image_exp)
        # remove the batch dimension, for batch is always 1 for inference.
        y_bboxes = decode_bbox(anchors_exp, y_bboxes_output)[0]
        y_cls = y_cls_output[0]
        # To speed up, do single class NMS, not multiple classes NMS.
        bbox_max_scores = np.max(y_cls, axis=1)
        bbox_max_score_classes = np.argmax(y_cls, axis=1)



        # keep_idx is the alive bounding box after nms.
        keep_idxs = single_class_non_max_suppression(y_bboxes,
                                                     bbox_max_scores,
                                                     conf_thresh=conf_thresh,
                                                     iou_thresh=iou_thresh,
                                                     )
        class_ids = []
        obj_ids = []
        for i, idx in enumerate(keep_idxs):
            #print(idx)
            conf = float(bbox_max_scores[idx])
            class_id = bbox_max_score_classes[idx]
            class_ids.append(class_id)
            bbox = y_bboxes[idx]
            # clip the coordinate, avoid the value exceed the image boundary.
            startX = max(0, int(bbox[0] * width))
            startY = max(0, int(bbox[1] * height))
            endX = min(int(bbox[2] * width), width)
            endY = min(int(bbox[3] * height), height)

            # construct a dlib rectangle object from the bounding
            # box coordinates and then start the dlib correlation
            # tracker
            #import pdb
            #pdb.set_trace()
            if class_id ==1:
                #offset_ += 1
                obj_ids.append("PersonID{}".format(str(i + offset_)))
                if not os.path.isdir('/images/' + obj_ids[i]):
                    print("I = ",i,"offset = ",offset_)
                    os.mkdir('/images/' +  obj_ids[i] + '/')
                    with open('/images/' +  obj_ids[i] + '/' + "label.txt","w") as f: f.write('False')

                tracker = dlib.correlation_tracker()
            # trackers_count += 1
                rect = dlib.rectangle(startX, startY, endX, endY)
                tracker.start_track(rgb, rect)
            # add the tracker to our list of trackers so we can
            # utilize it during skip frames
                trackers.append(tracker)
            else:
                obj_ids.append("PersonID{}".format(str(i + offset_)))
                #with open(csv_path,'a') as f :
                #    writer = csv.writer(f)
                #    row = []
                #    row.append(str(obj_ids[i]))
                #    row.append(time.time())
                #    row.append(True)
                    #import pdb;pdb.set_trace()
                #    writer.writerow(row)
                #

                
                if not os.path.isdir('/images/' +obj_ids[i]):
                    print("I = ",i,"offset = ",offset_)
                    os.mkdir('/images/' +  obj_ids[i] + '/')
                    with open('/images/' +  obj_ids[i] + '/' + "label.txt","w") as f: f.write('True')

                tracker = dlib.correlation_tracker()
            # trackers_count += 1
                rect = dlib.rectangle(startX, startY, endX, endY)
                tracker.start_track(rgb, rect)
            # add the tracker to our list of trackers so we can
            # utilize it during skip frames
                trackers.append(tracker)

        if keep_idxs: offset_+=1
    # otherwise, we should utilize our object *trackers* rather than
    # object *detectors* to obtain a higher frame processing throughput

    else:
        # loop over the trackers

        for i, tracker in enumerate(trackers):
            #print("TRACKERS",len(trackers))
            # set the status of our system to be 'tracking' rather
            # than 'waiting' or 'detecting'
            status = "Tracking"
            # update the tracker and grab the updated position
            tracker.update(rgb)
            pos = tracker.get_position()

            # unpack the position object
            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())
            # add the bounding box coordinates to the rectangles list
            rects.append((startX, startY, endX, endY))
            # print(class_ids[i])
            if class_ids[i] == 0:
                color = (0, 255, 0)
                image_resized = cv2.resize(rgb, (250, 250))
                #print(rgb.shape)
                image_np = rgb.astype(np.float32) / 255.0  #
                if totalFrames % 1 == 0:
                    # print(time.strftime("%Y:%m:%d:%H:%M:%S"))
                    try:
                        curr_time = time.strftime("%Y:%m:%d:%H:%M:%S")
                        cv2.imwrite('/images/'+obj_ids[i] + '/' + "image_{}.png".format(curr_time),
                                   rgb[startY:endY, startX:endX, ::-1])
                    except:
                        print("Error in line 248")
            else:
                color = (0, 0, 255)

                image_resized = cv2.resize(rgb, (250, 250))
                #print(rgb.shape)
                image_np = rgb.astype(np.float32) / 255.0  #
                if totalFrames % 1 == 0:
                    # print(time.strftime("%Y:%m:%d:%H:%M:%S"))
                    try:
                        curr_time = time.strftime("%Y:%m:%d:%H:%M:%S")
                        cv2.imwrite('/images/'+obj_ids[i] + '/' + "image_{}.png".format(curr_time),
                                   rgb[startY:endY, startX:endX, ::-1])
                    except:
                        print("Error in line 248")

                # image_exp = np.expand_dims(image_np, axis=0)

                # image_transposed = image_exp.transpose((0, 3, 1, 2))
                # feature = featurer.test(image_np)
                # print("Here")
                # feature = caffe_inference(face_verification_model, image_transposed)

                # for feature_ in features.values():  # person1
                #     # sim
                #     sim = cos_sim(feature, feature_)
                #     # sims.append((sim, int(tag), image_file1, image_file2))
                #     print(sim)

            #cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)
    trackers_count += len(trackers)
    #cv2.imshow("Frame", frame)
    #key = cv2.waitKey(1) & 0xFF
    #cv2.waitKey(1)

    totalFrames += 1
