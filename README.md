# Face  Image Clustering 

Implemented an application using TensorFlow that takes recorded video as input and captures images, verifies if the image is readable, and divide them into two clusters based on wearing mask.